﻿namespace oefening28_4
{
    internal class Provincie
    {
        private string _provincieNaam;
        private List<Gemeente> _gemeentes;


        public Provincie(string provincie)
        {
            ProvincieNaam = provincie;
        }

        public string ProvincieNaam
        {
            get { return _provincieNaam; }
            set { _provincieNaam = value; }
        }
        public List<Gemeente> Gemeentes
        {
            get { return _gemeentes; }
            set { _gemeentes = value; }
        }

        public void AddGemeente(Gemeente gemeente)
        {
            Gemeentes.Add(gemeente);
        }
        public void RemoveGemeente(Gemeente gemeente)
        {
            Gemeentes.Remove(gemeente);
        }
        public override string ToString()
        {
            string resultaat;
            resultaat = "Provincie " + ProvincieNaam + Environment.NewLine;
            foreach (Gemeente gemeente in Gemeentes)
            {
                resultaat += gemeente.ToString() + Environment.NewLine;
            }
            return resultaat;
        }
    }
}
