﻿namespace oefening28_4
{
    internal class Gemeente
    {
        private string _gemeenteNaam;
        private string _postcode;

        public Gemeente(string postecode, string gemeente)
        {
            Postcode = postecode;
            GemeenteNaam = gemeente;
        }
        public string GemeenteNaam
        {
            get { return _gemeenteNaam; }
            set { _gemeenteNaam = value; }
        }
        public string Postcode
        {
            get { return _postcode; }
            set { _postcode = value; }
        }
        public bool Equals(object obj)
        {
            bool resultaat = false;
            if (obj != null)
            {
                if (GetType() == obj.GetType())
                {
                    Gemeente gemeente = (Gemeente)obj;
                    if (this.GemeenteNaam == gemeente.GemeenteNaam && this.Postcode == gemeente.Postcode)
                    {
                        resultaat = true;
                    }
                }
            }
            return resultaat;
        }
        public override string ToString()
        {
            return $"{Postcode}  -  {GemeenteNaam}";
        }
    }
}
