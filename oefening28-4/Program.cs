﻿namespace oefening28_4
{
    internal class Program
    {
        static void Main(string[] args)
        {

            Provincie p = new Provincie("Antwerpen");
            Gemeente g1 = new Gemeente("2460", "Kasterlee");
            Gemeente g2 = new Gemeente("2440", "Geel");
            Gemeente g3 = new Gemeente("1000", "Brussel");
            Gemeente g4 = new Gemeente("2275", "Lille");
            List<Gemeente> listGemeentes = new List<Gemeente>();

            p.AddGemeente(g1);
            p.AddGemeente(g2);
            p.AddGemeente(g3);
            p.AddGemeente(g4);
            p.ToString();
        }
    }
}
